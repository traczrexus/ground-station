import sys

from core.LogAnalyzer import LogAnalyzer, get_tracz_path

from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import *
from python_qt_binding.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import random

from os import listdir
from os.path import isfile, join
import re
import time
import rospkg
import os
import signal

from google.protobuf.message import DecodeError

class LogAnalyzerGUI(QWidget):
    _type_str = ["mainboard", "sensorboard", "info"]
    _filetype_str = ["frame", "struct"]
    _yaxis_types = []
    _yaxis_types_to_plot = []
    _xaxis_types = ['timestamp', 'gs_timestamp']
    _line_types = ['solid', 'dot']
    _plot_datatype = ""

    def __init__(self, parent = None):
        super(LogAnalyzerGUI, self).__init__(parent)
        ui_file = os.path.join('ui',
                               'log_analyzer.ui')
        loadUi(ui_file, self)
        self.setWindowTitle("TRACZ Log Analyzer")


        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # # this is the Navigation widget
        # # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.mpl.addWidget(self.canvas)
        self.mpl.addWidget(self.toolbar)


        self._log_analyzer = LogAnalyzer()

        self.btn_file_log.clicked.connect(self.getfile_log)
        self.btn_file_dat.clicked.connect(self.getfile_dat)
        self.btn_combine.clicked.connect(self.combine)
        self.btn_plot.clicked.connect(self.plot)
        self.btn_save.clicked.connect(self.save)
        self.btn_load.clicked.connect(self.load)
        self.btn_add_y.clicked.connect(self.add_y)
        self.btn_clear_y.clicked.connect(self.clear_y)

        self.combo_yaxis.currentTextChanged.connect(self.reload_yranges_combo)

        for item in self._type_str:
            self.combo_type.addItem(item)

        for item in self._filetype_str:
            self.combo_filetype.addItem(item)

        for item in self._xaxis_types:
            self.combo_xaxis.addItem(item)

        for item in self._line_types:
            self.combo_line.addItem(item)

        self.btn_save.setEnabled(False)


    def getfile_log(self):
        fname = QFileDialog.getOpenFileName(self,
                                            'Open file',
                                            get_tracz_path("dummy"),
                                            "TRACZ LOG files (*.log)")

        fname = fname[0]
        if(len(fname)):
            self.actual_file.setText(str("{}".format(fname)))
            if "mf" in fname.lower():
                self.combo_filetype.setCurrentText("frame")
                self.combo_type.setCurrentText("mainboard")
            elif "sf" in fname.lower():
                self.combo_filetype.setCurrentText("frame")
                self.combo_type.setCurrentText("sensorboard")
            elif "ms" in fname.lower():
                self.combo_filetype.setCurrentText("struct")
                self.combo_type.setCurrentText("mainboard")
            elif "is" in fname.lower():
                self.combo_filetype.setCurrentText("struct")
                self.combo_type.setCurrentText("info")
            elif "ss" in fname.lower():
                self.combo_filetype.setCurrentText("struct")
                self.combo_type.setCurrentText("sensorboard")
            self._plot_datatype = "log"

    def getfile_dat(self):
        fname = QFileDialog.getOpenFileName(self,
                                            'Open file',
                                            get_tracz_path("dummy"),
                                            "TRACZ DAT files (*.csv)")

        fname = fname[0]
        if(len(fname)):
            self.actual_file.setText(str("{}".format(fname)))
            self._plot_datatype = "dat"


    def combine(self):
        mypath = QFileDialog.getExistingDirectory(self,
                                            'Open folder',
                                            get_tracz_path("dummy"))
        if len(mypath) > 0:
            self.combine_file(mypath, "MF_", "mf.log")
            self.combine_file(mypath, "SF_", "sf.log")
            self.combine_file(mypath, "MS_", "ms.log")
            self.combine_file(mypath, "IS_", "is.log")
            self.combine_file(mypath, "SS_", "ss.log")
            print("File combiner finished")

    def combine_file(self, core_path, files_pattern, file_out):
        all_files = [f for f in listdir(core_path) if isfile(join(core_path, f))]
        FILES = []

        for file in all_files:
            if files_pattern in file:
                FILES.append(file)

        FILES = sorted_nicely(FILES)
        if len(FILES):
            print(FILES)
            file_mother = open(core_path+"/"+file_out,"w")
            for file in FILES:
                f = open(core_path+"/"+file, "r")
                file_mother.write(f.read())
                f.close()
            file_mother.close()

    def load(self):
        self.log_data("Loading data ... ")
        if(self._log_analyzer.plot_struct_available()):
            try:
                fname =  self.actual_file.text()
                self.log_data("Reading {}".format(fname))
                actual_datatype = self._plot_datatype
                actual_filetype = self.combo_filetype.currentText()
                self.log_data("Datatype {}, filetype {}".format(actual_datatype, actual_filetype))
                if "log" == actual_datatype and "frame" == actual_filetype:
                    self.log_data("File type: log frame {}".format(self.combo_type.currentText()))
                    self.combo_xaxis.setCurrentIndex(self._xaxis_types.index('gs_timestamp'))
                    self.btn_save.setEnabled(True)
                    self._log_analyzer.load_log_frame(fname, self.combo_type.currentText())
                if "log" == actual_datatype and "struct" == actual_filetype:
                    self.combo_xaxis.setCurrentIndex(self._xaxis_types.index('timestamp'))
                    self.log_data("File type: log struct {}".format(self.combo_type.currentText()))
                    self.btn_save.setEnabled(True)
                    self._log_analyzer.load_log_struct(fname, self.combo_type.currentText())
                elif "dat" == actual_datatype:
                    self.combo_xaxis.setCurrentIndex(self._xaxis_types.index('timestamp'))
                    self.log_data("File type: dat")
                    self.btn_save.setEnabled(False)
                    self._log_analyzer.load_dat(fname)

                if self._log_analyzer.plot_struct_available():
                    plot_struct = self._log_analyzer.get_plot_struct()
                    self._yaxis_types = plot_struct['legend']
                    data_length = len(plot_struct['xdata'])
                    if data_length != 0:
                        data_s = (plot_struct['xdata'][-1]-plot_struct['xdata'][0])/data_length
                        data_hz = 1/data_s
                    else:
                        data_s = 0
                        data_hz = 0
                    self.log_data("Timestamp length {}, freq {:.0f}Hz, {:.4f}s".format(data_length,
                                                                                       data_hz,
                                                                                       data_s))
                    self.reload_yaxis_types()
                    self.reload_xranges([plot_struct['xdata'][0],
                                        plot_struct['xdata'][-1]])
                else:
                    self.log_data("ERROR struct is empty")
            except (IndexError, DecodeError) as exception:
                self.log_data("ERROR {}. Change datatype to other one.".format(exception))
        else:
            self.log_data("No plot data in memory!")
        self.log_data("Loading finished!")

    def save(self):
        fname =  self.actual_file.text()
        self.log_data("Saving file {} ...".format(fname))
        if(self._log_analyzer.plot_struct_available()):
            actual_filetype = self.combo_filetype.currentText()
            if actual_filetype == "frame":
                proposed_file = str(self.actual_file.text())
                # proposed_file = proposed_file[:-4]+"_mainboard.csv"
                # fname = QFileDialog.getSaveFileName(self,
                #                                     'Save file',
                #                                     proposed_file,
                #                                     "TRACZ DAT files (*.csv)")

                # fname = fname[0]
                # if(len(fname)):
                self._log_analyzer.save_plot_struct_to_dat( proposed_file[:-4]+"_mainboard.csv",
                                                           "mainboard")
                self._log_analyzer.save_plot_struct_to_dat( proposed_file[:-4]+"_sensorboard.csv",
                                                           "sensorboard")
                self._log_analyzer.save_plot_struct_to_dat( proposed_file[:-4]+"_info.csv",
                                                           "info")
            elif actual_filetype == "struct":
                proposed_file = str(self.actual_file.text())
                actual_type = self.combo_type.currentText()
                filename = proposed_file[:-4]+"_"+actual_type+".csv"
                self.log_data("saving struct data ... {}".format(filename))
                self._log_analyzer.save_plot_struct(filename)
            else:
                self.log_data("Cannot save data!")
        else:
            self.log_data("No plot data in memory!")
        self.log_data("Saving finished!")

    def plot(self):
        self.log_data("Plotting data ...")
        if(self._log_analyzer.plot_struct_available()):
            try:
                format_str = self.combo_line.currentText()
                if format_str == 'solid':
                    format_style = ['b-']
                elif format_str == 'dot':
                    format_style = ['b.']
                else:
                    format_style = ['b']
                # if self.spin_xmax.value() == 0:
                #     self._log_analyzer.plot_single_yaxis(self.combo_xaxis.currentText(),
                #                                          self.combo_yaxis.currentText(),
                #                                          formating)
                # else:
                if len(self._yaxis_types_to_plot) == 0:
                    plot_struct = self._log_analyzer.get_plot_struct_yaxis_single(
                                        self.combo_xaxis.currentText(),
                                        self.combo_yaxis.currentText())
                else:
                    plot_struct = self._log_analyzer.get_plot_struct_yaxis_multi(
                                        self.combo_xaxis.currentText(),
                                        self._yaxis_types_to_plot)

                self.plot_on_gui(plot_struct,
                                 formating=format_style,
                                 title=self.title.text(),
                                 xranges=self.get_xranges(),
                                 yranges=self.get_yranges())

                self.log_data("Plotting finished!")
            except ValueError as exeption:
                self.log_data(exeption)
        else:
            self.log_data("No plot data in memory!")


    def plot_on_gui(self, input_struct, formating=['b'], title="", #pylint: disable=too-many-arguments,dangerous-default-value
                    xranges=None, yranges=None):


        xdata = input_struct['xdata']
        ydata = input_struct['ydata']
        xlabel = input_struct['xlabel']
        ylabel = input_struct['ylabel']
        label_names = input_struct['legend']

        self.figure.clear()
        ax = self.figure.add_subplot(111)

        if len(label_names) > 1:
            for i in range(len(ydata)): #pylint: disable=consider-using-enumerate
                ax.plot(xdata, ydata[i], label=label_names[i])
        else:
            ax.plot(xdata, ydata, formating[0], label=label_names[0])
        # plt.plot(actual_x,actual_y,'k',label = 'real')
        # plt.xlim([-10,160])
        if xranges != None:
            ax.set_xlim([xranges[0], xranges[1]])

        if yranges != None:
            ax.set_ylim([yranges[0], yranges[1]])
        # # plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
        if len(label_names) > 1:
            ax.legend(loc=0, numpoints=1)
        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.grid()

        self.canvas.draw()

    def reload_yaxis_types(self):
        self.combo_yaxis.clear()
        for item in self._yaxis_types:
            self.combo_yaxis.addItem(item)

    def reload_xranges(self, ranges):
        self.spin_xmin.setRange(ranges[0], ranges[1])
        self.spin_xmax.setRange(ranges[0], ranges[1])
        self.spin_xmax.setValue(ranges[1])

    def reload_yranges(self, ranges):
        self.spin_ymin.setRange(-1000, 1000)
        self.spin_ymax.setRange(-1000, 1000)
        self.spin_ymin.setValue(ranges[0])
        self.spin_ymax.setValue(ranges[1])

    def reload_yranges_combo(self):
        if self.combo_yaxis.currentText() != '':
            data = self._log_analyzer.get_ydata(self.combo_yaxis.currentText())
            self.reload_yranges([min(data)-1, max(data)+1])

    def get_xranges(self):
        return [float("{0:.2f}".format(self.spin_xmin.value())),
                float("{0:.2f}".format(self.spin_xmax.value()))]

    def get_yranges(self):
        return [float("{0:.2f}".format(self.spin_ymin.value())),
                float("{0:.2f}".format(self.spin_ymax.value()))]

    def add_y(self):
        self._yaxis_types_to_plot.append(str(self.combo_yaxis.currentText()))
        self.label_y_axis.setText("Y axis: {}".format(self._yaxis_types_to_plot))

    def clear_y(self):
        self._yaxis_types_to_plot = []
        self.label_y_axis.setText("Y axis: {}".format(self._yaxis_types_to_plot))

    def log_data(self, text):
        self.output_console.moveCursor(QTextCursor.End)
        self.output_console.insertPlainText("{}\n".format(text))


def sorted_nicely( l ):
    """ Sorts the given iterable in the way that is expected.

    Required arguments:
    l -- The iterable to be sorted.

    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)

def main():
    app = QApplication(sys.argv)
    ex = LogAnalyzerGUI()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    main()
