import time
import os
import rospy

class Logger(object):
    _log_file = None
    _log_filename = None
    _tag_with = 20

    def __init__(self, roslog=True):
        self._roslog = roslog

    def file_open_write(self, filename):
        timestr = time.strftime("%Y-%m-%d-%H-%M-%S")
        self._log_filename = filename+"_gs_"+timestr+".log"
        self._log_filename = os.path.join(os.path.expanduser("~"),
                                          '.tracz',
                                          self._log_filename)
        if not os.path.exists(os.path.dirname(self._log_filename)):
            os.makedirs(os.path.dirname(self._log_filename))
        self._log_file = open(self._log_filename, 'w')

    def file_open_read(self, filename):
        self._log_file = open(filename, 'r')

    def get_filename(self):
        return self._log_filename

    def file_close(self):
        if self._log_file != None:
            self._log_file.close()

    def file_write(self, data):
        self._log_file.write(data)

    def file_read(self, read_bytes=1):
        return self._log_file.read(read_bytes)

    def debug(self, log_tag, msg): #pylint: disable=no-self-use
        if(self._roslog):
            rospy.logdebug("%s: %s", log_tag, msg)
        else:
            print("[DEBUG]   {: <{width}} {}".format(log_tag+":", msg, width=self._tag_with))

    def info(self, log_tag, msg): #pylint: disable=no-self-use
        if(self._roslog):
            rospy.loginfo("%s: %s", log_tag, msg)
        else:
            print("[INFO]    {: <{width}} {}".format(log_tag+":", msg, width=self._tag_with))

    def warning(self, log_tag, msg): #pylint: disable=no-self-use
        if(self._roslog):
            rospy.logwarn("%s: %s", log_tag, msg)
        else:
            print("[WARNING] {: <{width}} {}".format(log_tag+":", msg, width=self._tag_with))

    def error(self, log_tag, msg): #pylint: disable=no-self-use
        if(self._roslog):
            rospy.logerr("%s: %s", log_tag, msg)
        else:
            print("[ERROR]   {: <{width}} {}".format(log_tag+":", msg, width=self._tag_with))

    def fatal(self, log_tag, msg): #pylint: disable=no-self-use
        if(self._roslog):
            rospy.logfatal("%s: %s", log_tag, msg)
        else:
            print("[FATAL]   {: <{width}} {}".format(log_tag+":", msg, width=self._tag_with))
