import struct
import os
import matplotlib.pyplot as plt

from core.SerialPortAdapter import SerialPortAdapter
from core.MessageDispatcher import MessageDispatcher
from core.Facade import SerializerFacade
from core.Logger import Logger

from tracz_types import topics_mainboard_types, topics_sensorboard_types, \
            topics_info_types

from google.protobuf.message import DecodeError

LOGGER = Logger(roslog=False)
LOG_TAG = "LogAnalyzer"

class LogAnalyzer(object):
    _tracz_mainboard = {}
    _tracz_sensorboard = {}
    _tracz_info = {}
    _facade = SerializerFacade()
    _msg_cnt = [0, 0, 0] #mainboard, sensorboard, info
    _gs_timestamp_mb = 0.0
    _gs_timestamp_sb = 0.0
    _gs_timestamp_info = 0.0


    _plot_struct = {
        'xdata' : [],
        'ydata' : [],
        'xlabel': "",
        'ylabel': "",
        'legend': []
    }

    def __init__(self):
        self._handlers_incoming = MessageDispatcher._handlers_incoming #pylint: disable=protected-access


    def init_data(self):
        self._tracz_mainboard = {}
        self._tracz_sensorboard = {}
        self._tracz_info = {}
        self._tracz_mainboard['msg_cnt'] = []
        self._tracz_mainboard['gs_timestamp'] = []
        self._tracz_sensorboard['msg_cnt'] = []
        self._tracz_sensorboard['gs_timestamp'] = []
        self._tracz_info['msg_cnt'] = []
        self._tracz_info['gs_timestamp'] = []


        for value in topics_mainboard_types.itervalues():
            self._tracz_mainboard[value[1]] = []

        for value in topics_sensorboard_types.itervalues():
            self._tracz_sensorboard[value[1]] = []

        for value in topics_info_types.itervalues():
            self._tracz_info[value[1]] = []

    def decode_frame(self, frame):
        if frame != None:
            message_id = frame[0]
            if message_id in self._handlers_incoming.keys():
                message = frame[1:]
                message_cnt = frame[1]
                if message_id == '\x01':
                    self.compare_msg_cnt(frame[1], 0)
                    decoded_message_mb = self._facade.decode_main_board(message[1:])
                    self._tracz_mainboard['msg_cnt'].append(float(struct.unpack("B", message_cnt)[0]))
                    for value in topics_mainboard_types.itervalues():
                        self._tracz_mainboard[value[1]].append(getattr(decoded_message_mb,
                                                                       value[1]))
                    self.update_gs_timestamp(self._tracz_mainboard)

                elif message_id == '\x02':
                    self.compare_msg_cnt(frame[1], 1)
                    decoded_message_sb = self._facade.decode_sensor_board(message[1:])
                    self._tracz_sensorboard['msg_cnt'].append(float(struct.unpack("B", message_cnt)[0]))
                    for value in topics_sensorboard_types.itervalues():
                        self._tracz_sensorboard[value[1]].append(getattr(decoded_message_sb,
                                                                         value[1]))
                    self.update_gs_timestamp(self._tracz_sensorboard)

                elif message_id == '\x03':
                    self.compare_msg_cnt(frame[1], 2)
                    decoded_message_info = self._facade.decode_info(message[1:])
                    self._tracz_info['msg_cnt'].append(float(struct.unpack("B", message_cnt)[0]))
                    for value in topics_info_types.itervalues():
                        self._tracz_info[value[1]].append(getattr(decoded_message_info,
                                                                  value[1]))
                    self.update_gs_timestamp(self._tracz_info)

    def update_gs_timestamp(self, datatype):
        if datatype == self._tracz_mainboard:
            gs_timestamp = self._gs_timestamp_mb
        elif datatype == self._tracz_sensorboard:
            gs_timestamp = self._gs_timestamp_sb
        elif datatype == self._tracz_info:
            gs_timestamp = self._gs_timestamp_info

        if datatype['gs_timestamp']:
            if datatype['timestamp'][-1] < datatype['timestamp'][-2]:
                # LOGGER.debug(LOG_TAG, "Timestamp {} Internal clock {}".format(datatype['timestamp'][-1], datatype['timestamp'][-2]))
                # LOGGER.debug(LOG_TAG, "Type {}".format(type(datatype)))
                if datatype == self._tracz_mainboard:
                    self._gs_timestamp_mb = datatype['gs_timestamp'][-1]
                    gs_timestamp = self._gs_timestamp_mb
                elif datatype == self._tracz_sensorboard:
                    self._gs_timestamp_sb = datatype['gs_timestamp'][-1]
                    gs_timestamp = self._gs_timestamp_sb
                elif datatype == self._tracz_info:
                    self._gs_timestamp_info = datatype['gs_timestamp'][-1]
                    gs_timestamp = self._gs_timestamp_info
        datatype['gs_timestamp'].append(gs_timestamp + datatype['timestamp'][-1])
        # LOGGER.debug(LOG_TAG, "Timestamp {} Internal clock {}".format(datatype['timestamp'][-1], datatype['gs_timestamp'][-1]))

    def compare_msg_cnt(self, actual, predicted):
        msg_cnt_actual = struct.unpack("B", actual)[0]
        type_str = ""
        if predicted == 0:
            type_str = "mainboard"
        elif predicted == 1:
            type_str = "sensorboard"
        elif predicted == 2:
            type_str = "info"
        if(self._msg_cnt[predicted] != msg_cnt_actual):
            LOGGER.fatal(LOG_TAG,
                         "msg_cnt {} actual {} predicted {}".format(type_str,
                                                                    msg_cnt_actual,
                                                                    self._msg_cnt[predicted]))
            self._msg_cnt[predicted] = msg_cnt_actual
        self._msg_cnt[predicted] += 1
        if self._msg_cnt[predicted] > 255:
            self._msg_cnt[predicted] = 0

    def load_log_frame(self, filename, datatype):
        self.clean_data()
        # self.save_to_file(filename, datatype)
        LOGGER.debug(LOG_TAG, filename)
        # LOGGER.debug(LOG_TAG, yaxis)

        serial_port_adapter = SerialPortAdapter("dummy", '38400', roslog=False)
        data = serial_port_adapter.decode_log(filename)

        self._gs_timestamp_mb = 0.0
        self._gs_timestamp_sb = 0.0
        self._gs_timestamp_info = 0.0
        # LOGGER.debug(LOG_TAG, data)
        if(len(data)):
            for item in data:
                try:
                    self.decode_frame(item)
                except DecodeError as exception:
                    LOGGER.fatal(LOG_TAG, "{} Frame is corrupted.".format(exception))

            if datatype == "mainboard":
                self.decoded_log_to_plot_struct(self._tracz_mainboard, 'gs_timestamp')
            elif datatype == "sensorboard":
                self.decoded_log_to_plot_struct(self._tracz_sensorboard, 'gs_timestamp')
            elif datatype == "info":
                self.decoded_log_to_plot_struct(self._tracz_info, 'gs_timestamp')

        else:
            LOGGER.error(LOG_TAG, "No data to decode")

    def load_log_struct(self, filename, datatype):
        self.clean_data()
        file_handler = open(filename, 'rb')
        binary_data = file_handler.read()
        if datatype == "mainboard":
            coding = 'BBBBBBBBfBBffffHHfBBfffBBHHfBBBBBBBBI'
            legend = ["SODS", "LO", "SOE", "ambient_valve_state", "tank_valve_state",
                      "mobius_1_state", "mobius_2_state", "mobius_3_state", "motor_current",
                      "motor_current_raw[0]", "motor_current_raw[0]", "motor_velocity",
                      "motor_position", "desired_position", "desired_current", "pwm_in1_duty",
                      "pwm_in2_duty", "differential_pressure", "differential_pressure_raw_data[0]",
                      "differential_pressure_raw_data[1]", "desired_differential_pressure",
                      "load_cell_force", "desired_force", "mb_msg_counter", "info_msg_counter",
                      "adc_raw_data[0]", "adc_raw_data[1]", "timestamp", "gripper_in_home_position",
                      "state_12v", "electrolock_state", "tv_channel_state", "illumination_state",
                      "card_detect", "data_logging", "sd_card_has_enough_space", "error_info"]
        elif datatype == "sensorboard":
            coding = 'BfffBBBBBBfffBBBBBBffBBBBffBBBBBBBfBBBI'
            legend = ["SODS", "acceleration_x", "acceleration_y", "acceleration_z",
                      "acc_raw_data[0]", "acc_raw_data[1]", "acc_raw_data[2]",
                      "acc_raw_data[3]", "acc_raw_data[4]", "acc_raw_data[5]",
                      "angular_velocity_x", "angular_velocity_y", "angular_velocity_z",
                      "gyro_raw_data[0]", "gyro_raw_data[1]", "gyro_raw_data[2]",
                      "gyro_raw_data[3]", "gyro_raw_data[4]", "gyro_raw_data[5]",
                      "absolute_pressure", "pressure_sensor_temperature",
                      "absolute_pressure_raw_data[0]",
                      "absolute_pressure_raw_data[1]", "absolute_pressure_raw_data[2]",
                      "absolute_pressure_raw_data[3]", "temperature", "humidity",
                      "temperature_humidity_raw_data[0]", "temperature_humidity_raw_data[1]",
                      "temperature_humidity_raw_data[2]", "temperature_humidity_raw_data[3]",
                      "temperature_humidity_raw_data[4]", "temperature_humidity_raw_data[5]",
                      "msg_counter", "timestamp", "data_logging", "card_detect",
                      "sd_card_has_enough_space", "error_info"]
        elif datatype == "info":
            coding = 'BBBBBBBBfBf'
            legend = ["mode", "experiment_state", "commands_task_status", "motor_control_mode",
                      "can_switch_to_test_mode", "timed_valves_operation", "emergency_status",
                      "current_command_id", "current_command_argument", "release_air_at_the_end",
                      "timestamp"]

        self.decoded_struct_to_plot_struct(binary_data, coding, legend)

    def load_dat(self, filename, split_format=','):
        self.clean_data()
        self._plot_struct = read_data(filename, split_format)

    def save_plot_struct_to_dat(self, filename, datatype):
        if datatype == "mainboard":
            save_data(self._tracz_mainboard, filename)
        if datatype == "sensorboard":
            save_data(self._tracz_sensorboard, filename)
        if datatype == "info":
            save_data(self._tracz_info, filename)

    def get_plot_struct(self):
        return self._plot_struct

    def save_plot_struct(self, filename):
        LOGGER.debug("!!!!!", "Save plot struct")
        fhandler = open(filename, 'w')
        separator = ","
        # for key in self._plot_struct.iterkeys():
        #     LOGGER.debug(LOG_TAG, key)


        header = ["timestamp"] + self._plot_struct['legend']
        # LOGGER.debug(LOG_TAG, header)
        line_str = separator.join(header)
        fhandler.write("{}\n".format(line_str))


        # LOGGER.debug(LOG_TAG, self._plot_struct['legend'][0])
        # LOGGER.debug(LOG_TAG, self._plot_struct['ydata'][0])

            # LOGGER.debug(LOG_TAG, line)
        # header_list = []
        for line_number in range(len(self._plot_struct['xdata'])):
            line = [str(self._plot_struct['xdata'][line_number])]
            # LOGGER.debug(LOG_TAG, line_number)
            for value in self._plot_struct['ydata']:
                line.append(str(value[line_number]))
            # LOGGER.debug(LOG_TAG, line)
            line_str = separator.join(line)
            fhandler.write("{}\n".format(line_str))

        fhandler.close()

    def clean_data(self):
        self.init_data()
        self._plot_struct = {'xdata' : [],
                             'ydata' : [],
                             'xlabel': "",
                             'ylabel': "",
                             'legend': []}

    def get_plot_struct_yaxis_single(self, xaxis, yaxis):
        new_struct = {}
        new_struct['xdata'] = self._plot_struct['xdata']
        new_struct['ydata'] = self._plot_struct['ydata'][self._plot_struct['legend'].index(yaxis)]
        new_struct['ylabel'] = yaxis
        new_struct['xlabel'] = xaxis
        new_struct['legend'] = [yaxis]

        return new_struct

    def get_plot_struct_yaxis_multi(self, xaxis, yaxis):
        new_struct = {}
        new_struct['xdata'] = self._plot_struct['xdata']
        new_yaxis = []
        for item in yaxis:
            new_yaxis.append(self._plot_struct['ydata'][self._plot_struct['legend'].index(item)])
        new_struct['ydata'] = new_yaxis
        new_struct['xlabel'] = xaxis
        new_struct['legend'] = yaxis
        new_struct['ylabel'] = yaxis[0]

        return new_struct

    def get_legend(self):
        return self._plot_struct['legend']

    def get_xdata(self):
        return self._plot_struct['xdata']

    def get_ydata(self, item=None):
        # if item != None:
        #     return self._plot_struct['ydata']
        # else:
        return self._plot_struct['ydata'][self._plot_struct['legend'].index(item)]

    def plot_single_yaxis(self, xaxis, yaxis, formating, xranges=None, yranges=None):
        plot_data(input_struct=self.get_plot_struct_yaxis_single(xaxis, yaxis),
                  formating=formating,
                  xranges=xranges,
                  yranges=yranges)


    def decoded_log_to_plot_struct(self, datatype, xaxis_label="timestamp"):
        self._plot_struct['xdata'] = datatype[xaxis_label]
        self._plot_struct['xlabel'] = xaxis_label
        for key, value in datatype.iteritems():
            self._plot_struct['legend'].append(key)
            self._plot_struct['ydata'].append(value)
        self._plot_struct['ylabel'] = self._plot_struct['legend'][0]

    def decoded_struct_to_plot_struct(self, binary_data, coding, legend):
        if len(coding) == len(legend):
            decoded_data = struct.Struct(coding)
            size = decoded_data.size
            if "timestamp" in legend:
                xlabel = "timestamp"
            else:
                xlabel = legend[0]
            ydata = [[] for i in range(len(coding))]#pylint: disable=unused-variable
            timestamp = []
            while(len(binary_data) >= size):
                binary_packet = binary_data[:size]
                binary_data = binary_data[size:]
                fields = decoded_data.unpack(binary_packet)
                for item in range(len(fields)):
                    ydata[item].append(fields[item])
                timestamp.append(fields[legend.index(xlabel)])
            if len(binary_data) != 0:
                LOGGER.error(LOG_TAG, "Loading binary data length error. Length {}, Left {}".format(len(binary_packet),
                                                                                                    len(binary_data)))
            self._plot_struct['xdata'] = timestamp
            self._plot_struct['ydata'] = ydata
            self._plot_struct['xlabel'] = "timestamp"
            self._plot_struct['ylabel'] = legend[0]
            self._plot_struct['legend'] = legend


    def plot_struct_available(self):
        return bool(self._plot_struct)


##################################################################################


def get_tracz_path(filename):
    log_filename = os.path.join(os.path.expanduser("~"),
                                '.tracz',
                                filename)
    if not os.path.exists(os.path.dirname(log_filename)):
        os.makedirs(os.path.dirname(log_filename))

    return log_filename

# def plot_data_from_file(fname="mainboard.log", yaxis="timestamp"):
    # output_struct = read_data(fname, split_format=",")
    # plot_data_from_struct(output_struct, yaxis)


def save_data(data_type, filename, xdata_label="gs_timestamp"):
    fhandler = open(filename, 'w')
    # LOGGER.debug(LOG_TAG, len(self._tracz_mainboard['timestamp']))
    separator = ","
    # for key in self._tracz_mainboard.iterkeys():

        # LOGGER.debug(LOG_TAG, line)
    header_list = []
    for item in range(len(data_type[xdata_label])):
        line = []
        if len(line) == 0:
            header_list.append(xdata_label)
            line.append(str(data_type[xdata_label][item]))

        for key, value in data_type.iteritems():
            if item == 0:
                header_list.append(str(key))
            line.append(str(float(value[item])))
        # LOGGER.debug(LOG_TAG, line)
        if item == 0:
            header = separator.join(header_list)
            # LOGGER.debug(LOG_TAG, header)
            # LOGGER.debug(LOG_TAG, header_list)
            fhandler.write("{}\n".format(header))
        line_str = separator.join(line)
        # LOGGER.debug(LOG_TAG, line)
        fhandler.write("{}\n".format(line_str))

    fhandler.close()




def read_data(fname, split_format=""):
    lines = tuple(open(fname, 'r'))
    lines = [x.strip() for x in lines]

    xdata = []
    ydata = []

    legend = []

    if split_format == "":
        line = lines[0].split()
    else:
        line = lines[0].split(split_format)
    # LOGGER.info(LOG_TAG, line)
    # timestamp_position = line.index('timestamp')
    # LOGGER.info(LOG_TAG, timestamp_position)
    xlabel = line[0]
    ylabel = line[1]
    if len(line[1:]) >= 1:
        legend = line[1:]

    lines = lines[1:]

    for i in range(len(lines)): #pylint: disable=consider-using-enumerate
        if split_format == "":
            line = lines[i].split()
        else:
            line = lines[i].split(split_format)
        xdata.append(float(line[0]))
        if len(line) > 2:
            if i == 0:
                for j in range(1, len(line)):
                    ydata.append([])
            for j in range(1, len(line)):
                ydata[j-1].append(float(line[j]))
        else:
            ydata.append(float(line[1]))

    output_struct = {
        'xdata' : xdata,
        'ydata' : ydata,
        'xlabel': xlabel,
        'ylabel': ylabel,
        'legend': legend
    }
    return output_struct

def plot_data(input_struct, formating=[], title="", #pylint: disable=too-many-arguments,dangerous-default-value
              xranges=None, yranges=None):

    xdata = input_struct['xdata']
    ydata = input_struct['ydata']
    xlabel = input_struct['xlabel']
    ylabel = input_struct['ylabel']
    label_names = input_struct['legend']

    if len(label_names) > 1:
        for i in range(len(ydata)):
            label_names.append("")
        for i in range(len(ydata)): #pylint: disable=consider-using-enumerate
            plt.plot(xdata, ydata[i], formating[i], label=label_names[i])

    else:
        plt.plot(xdata, ydata, formating[0], label=label_names[0])
    # plt.plot(actual_x,actual_y,'k',label = 'real')
    # plt.xlim([-10,160])
    if xranges != None:
        plt.xlim(xranges[0], xranges[1])

    if yranges != None:
        plt.ylim(yranges[0], yranges[1])
    # # plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
    if len(label_names) > 1:
        plt.legend(loc=0, numpoints=1)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # if save_to_file == 'T' or save_to_file == '':
    #     plt.savefig(fig_name+".png",
    #                 format = 'png'
    #                 )
    #     plt.clf()
    # else:
    plt.show()
