import time
import struct
import threading
import serial

from PyCRC.CRC16 import CRC16
from cobs import cobs

from core.Logger import Logger

LOG_TAG = "SerialPortAdapter"

class SerialPortAdapter(object):

    _port_is_closed = True
    _serial_port = None

    _syncByte = '\x00'
    _bits_read = 0.0
    _bits_read_interval = 1.0
    _bits_rate = 0.0
    _read_buffer = ""

    def __init__(self, port_name, baud, roslog=True):
        self._port_name = port_name
        self._baud = baud
        self._logger = Logger(roslog)
        self.compute_bitrate_thread = threading.Thread(target=self.compute_downlink)
        self.compute_bitrate_thread.setDaemon(True)
        self.compute_bitrate_thread.start()

    def run(self):
        show_error_message = True
        while self._port_is_closed:
            try:
                self._serial_port = serial.Serial(self._port_name,
                                                  self._baud)
                self._port_is_closed = False
                self._logger.info(LOG_TAG,
                                  "Port {} is opened".format(self._port_name))
            except serial.SerialException:
                time.sleep(1)
                self._port_is_closed = True
                if show_error_message is True:
                    self._logger.error(LOG_TAG,
                                       "Cannot open the port {}".format(self._port_name))
                    show_error_message = False

        self._logger.file_open_write("tracz")
        self._logger.info(LOG_TAG, "Logging data to {}".format(self._logger.get_filename()))
        # self._statistics = [0, 0, 0, 0]

    def close(self):
        self._logger.info(LOG_TAG, "Closing serial port adapter {}".format(self._port_name))
        if self._port_is_closed is False:
            self._serial_port.close()
        self._logger.file_close()
        self._logger.info(LOG_TAG, "Data logged to {}".format(self._logger.get_filename()))

        # self._logger.warning(LOG_TAG, self._statistics)

    def decode_log(self, filename):
        # self._statistics = [0,0,0,0]
        self._logger.info(LOG_TAG, "Decoding logs from {}".format(filename))
        self._logger.file_open_read(filename)
        data = []
        while True:
            try:
                byte = self._logger.file_read(1)
                if len(byte) == 0:
                    break
                if byte == self._syncByte:
                    if len(self._read_buffer):
                        # self._logger.info(LOG_TAG, "read buffer {}".format(self._read_buffer))
                        data.append(self.decode_cobs(self._read_buffer))
                else:
                    self._read_buffer = self._read_buffer+byte

            except cobs.DecodeError as exception:
                self._logger.error(LOG_TAG,
                                   "COBS ERROR: {}".format(exception))
                self._read_buffer = ""
            except Exception as exception: #pylint: disable=broad-except
                self._logger.error(LOG_TAG,
                                   "Cannot decode the frame: {}".format(exception))
                self._read_buffer = ""

        self._logger.file_close()
        self._logger.info(LOG_TAG, "Decoding finished successfully")
        # self._logger.warning(LOG_TAG, self._statistics)
        return data

    def send(self, raw_bytes):
        # self._logger.warning(LOG_TAG,"raw_bytes")
        # for value in enumerate(raw_bytes):
        #     self._logger.warning(LOG_TAG,"0x{:02x} ".format(struct.unpack("B", value[1])[0]))
        # self._logger.warning(LOG_TAG,"")
        crc16 = CRC16().calculate(raw_bytes)
        frame_send = "{}{}".format(raw_bytes,
                                   struct.pack("H", crc16))
        frame_cobs = "{}{}{}".format(self._syncByte,
                                     cobs.encode(frame_send),
                                     self._syncByte)
        # self._logger.warning(LOG_TAG, "frame_send")
        # for value in enumerate(frame_cobs):
        #     self._logger.warning(LOG_TAG,"0x{:02x} ".format(struct.unpack("B", value[1])[0]))
        # self._logger.warning(LOG_TAG,"")

        self._serial_port.write(frame_cobs)


    def receive(self):
        try:
            while not self._port_is_closed:
                byte = self._serial_port.read(1)
                self._bits_read += 8.0
                self._logger.file_write(byte)
                # self._logger.warning(LOG_TAG,"byte {:02x}".format(struct.unpack("B", byte)[0]))
                if byte == self._syncByte:
                    if len(self._read_buffer):
                        # self._logger.warning(LOG_TAG,"decode_cobs")
                        return self.decode_cobs(self._read_buffer)
                else:
                    self._read_buffer = self._read_buffer+byte

                # self._logger.warning(LOG_TAG,"read_buffer")
                # for value in enumerate(self._read_buffer):
                #     self._logger.warning(LOG_TAG,
                #                          "0x{:02x} ".format(struct.unpack("B", value[1])[0]))
                # self._logger.warning(LOG_TAG,"")
        except cobs.DecodeError as exception:
            self._logger.error(LOG_TAG,
                               "COBS ERROR: {}".format(exception))
            self._read_buffer = ""
        except Exception as exception: #pylint: disable=broad-except
            self._logger.error(LOG_TAG,
                               "Cannot decode the frame: {}".format(exception))
            if "device disconnected" in str(exception):
                self._port_is_closed = True
            self._read_buffer = ""

    def decode_cobs(self, frame_raw):
        # self._logger.warning(LOG_TAG,"frame")
        # for value in enumerate(frame_raw):
        #     self._logger.warning(LOG_TAG,"0x{:02x} ".format(struct.unpack("B", value[1])[0]))
        # self._logger.warning(LOG_TAG,"")

        frame = cobs.decode(frame_raw)

        # self._logger.warning(LOG_TAG,"frame_decoded")
        # for value in enumerate(frame):
        #     self._logger.warning(LOG_TAG,"0x{:02x} ".format(struct.unpack("B", value[1])[0]))
        # self._logger.warning(LOG_TAG,"")

        msg_id = frame[0]

        # self._logger.warning(LOG_TAG,"msg id {:02x}".format(struct.unpack("B", msg_id)[0]))
        # self._logger.warning(LOG_TAG,"msg id {:02x}".format(int(msg_id)))
        # if struct.unpack("B", msg_cnt)[0] > 10:
        #     self._port_is_closed = True

        msg_cnt = frame[1]

        # self._statistics[struct.unpack("B", msg_id)[0]] += 1

        # if struct.unpack("B", msg_cnt)[0] > 50:
        #     self._port_is_closed = True
        # self._logger.warning(LOG_TAG,"msg_cnt {:02x}".format(struct.unpack("B", msg_cnt)[0]))
        msg = frame[2:-2]
        # self._logger.warning(LOG_TAG,"msg {}".format(msg))
        # self._logger.warning(LOG_TAG,"msg")
        # for value in enumerate(msg):
        #     self._logger.warning(LOG_TAG,"0x{:02x} ".format(struct.unpack("B", value[1])[0]))
        # self._logger.warning(LOG_TAG,"")
        frame_send = "{}{}{}".format(msg_id,
                                     msg_cnt,
                                     msg)
        crc16 = struct.unpack("H", frame[-2:])[0]
        crc16_check = CRC16().calculate(frame_send)
        # self._logger.warning(LOG_TAG,
        #                      "crc16 {:02x}. crc16_check {:02x}".format(crc16, crc16_check))
        self._read_buffer = ""

        if crc16 != crc16_check:
            self._logger.error(LOG_TAG, "CRC16 error")
            self._logger.error(LOG_TAG,
                               "crc16 {:02x}. crc16_check {:02x}".format(crc16, crc16_check))
        else:
            return frame_send

    def is_port_closed(self):
        return self._port_is_closed

    def compute_downlink(self):
        starttime = time.time()
        while True:
            self._bits_rate = self._bits_read/self._bits_read_interval
            self._bits_read = 0
            time.sleep(self._bits_read_interval
                       - ((time.time() - starttime) % self._bits_read_interval))

    def get_bitrate(self):
        return self._bits_rate
