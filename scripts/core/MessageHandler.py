import abc
import rospy
import tf

from tracz_types import topics_mainboard_types, topics_sensorboard_types, \
                        topics_info_types
from core.Facade import SerializerFacade


class HandlerBaseIncoming: #pylint: disable=no-init,too-few-public-methods,super-init-not-called
    __metaclass__ = abc.ABCMeta

    _facade = SerializerFacade()

    @abc.abstractmethod
    def decode(self, message):
        pass


class MotorboardMessageHandler(HandlerBaseIncoming): #pylint: disable=no-init,too-few-public-methods,too-many-instance-attributes
    _topics = {}
    _load_cell = 0.0

    def __init__(self):
        for key, value in topics_mainboard_types.iteritems():
            self._topics[key] = [rospy.Publisher(key, value[0], queue_size=1),
                                 value[0]()]

    def decode(self, message):
        decoded_message = HandlerBaseIncoming._facade.decode_main_board(message[1:])

        for key, value in self._topics.iteritems():
            # if key == '/load_cell/force':
            #     load_cell_actual = getattr(decoded_message, topics_mainboard_types[key][1])
            #     self._load_cell = 0.1 * load_cell_actual + 0.9* self._load_cell
            #     value[1].data = self._load_cell
            #     value[0].publish(value[1])
            # else:
            value[1].data = getattr(decoded_message, topics_mainboard_types[key][1])
            value[0].publish(value[1])
            if key == '/motor/position/actual':
                transform_broadcaster = tf.TransformBroadcaster()
                # value[1] is in mm and should be meters for tf
                transform_broadcaster.sendTransform((-0.025, 0, (value[1].data/1000)),
                                                    (0, 0, 0, 1),
                                                    rospy.get_rostime(),
                                                    "foot",
                                                    "base_link")


class SensorboardMessageHandler(HandlerBaseIncoming): #pylint: disable=no-init,too-few-public-methods
    _topics = {}

    def __init__(self):
        for key, value in topics_sensorboard_types.iteritems():
            self._topics[key] = [rospy.Publisher(key, value[0], queue_size=1),
                                 value[0]()]

    def decode(self, message): #pylint: disable=no-init,too-few-public-methods
        decoded_message = HandlerBaseIncoming._facade.decode_sensor_board(message[1:])

        for key, value in self._topics.iteritems():
            value[1].data = getattr(decoded_message, topics_sensorboard_types[key][1])
            value[0].publish(value[1])

class InfoMessageHandler(HandlerBaseIncoming): #pylint: disable=no-init,too-few-public-methods
    _topics = {}

    def __init__(self):
        for key, value in topics_info_types.iteritems():
            self._topics[key] = [rospy.Publisher(key, value[0], queue_size=1),
                                 value[0]()]

    def decode(self, message): #pylint: disable=no-init,too-few-public-methods
        decoded_message = HandlerBaseIncoming._facade.decode_info(message[1:])
        for key, value in self._topics.iteritems():
            value[1].data = getattr(decoded_message, topics_info_types[key][1])
            value[0].publish(value[1])

class HandlerBaseOutgoing: #pylint: disable=no-init,too-few-public-methods,super-init-not-called
    __metaclass__ = abc.ABCMeta

    _facade = SerializerFacade()

    @abc.abstractmethod
    def encode(self, message):
        pass


class CommandListBoolHandler(HandlerBaseOutgoing): #pylint: disable=no-init,too-few-public-methods
    def encode(self, message):
        return message


class CommandListFloatHandler(HandlerBaseOutgoing): #pylint: disable=no-init,too-few-public-methods
    def encode(self, message):
        encoded_message = HandlerBaseOutgoing._facade.encode_command_list(message)
        return encoded_message
