from protocol_buffer.tracz_mainboard_pb2 import tracz_mainboard_msg
from protocol_buffer.tracz_sensorboard_pb2 import tracz_sensorboard_msg
from protocol_buffer.tracz_commandlist_float_pb2 import tracz_commandlist_float_msg
from protocol_buffer.tracz_info_pb2 import tracz_info_msg

class SerializerFacade(object):
    def __init__(self):
        self.msg_mainboard = tracz_mainboard_msg()
        self.msg_sensorboard = tracz_sensorboard_msg()
        self.msg_commandlist_float = tracz_commandlist_float_msg()
        self.msg_info = tracz_info_msg()

    def decode_main_board(self, message):
        self.msg_mainboard.ParseFromString(message)
        return self.msg_mainboard

    def decode_sensor_board(self, message):
        self.msg_sensorboard.ParseFromString(message)
        return self.msg_sensorboard

    def decode_info(self, message):
        self.msg_info.ParseFromString(message)
        return self.msg_info

    def encode_command_list(self, message):
        self.msg_commandlist_float.data = float(message)
        return self.msg_commandlist_float.SerializeToString()
