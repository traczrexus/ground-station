import threading
import struct
import time
import rospy

from core.SerialPortAdapter import SerialPortAdapter
from core.MessageHandler import MotorboardMessageHandler, \
    SensorboardMessageHandler, InfoMessageHandler, \
    CommandListBoolHandler, CommandListFloatHandler
from core.Logger import Logger

from tracz_types import tracz_actions_types
from tracz_ground_station.srv import CommandListBool, CommandListFloat

from std_msgs.msg import UInt32, Float64, Header

LOG_TAG = "MessageDispatcher"


class MessageDispatcher(object):

    _handlers_incoming = {'\x01': MotorboardMessageHandler(),
                          '\x02': SensorboardMessageHandler(),
                          '\x03': InfoMessageHandler()}
    _handlers_outgoing = {}
    _services_handlers = {}
    _serial_port_adapter = None
    _services = {}

    _command_list_actions = {}

    _msg_send_cnt = 0
    _experiment_state = 0

    def __init__(self, port_name='/dev/tracz'):
        self._serial_port_adapter = SerialPortAdapter(port_name,
                                                      '38400')
        self._logger = Logger()
        for key, values in tracz_actions_types.iteritems():
            if values[1] == CommandListBool:
                self._handlers_outgoing[key] = CommandListBoolHandler()
            else:
                self._handlers_outgoing[key] = CommandListFloatHandler()
            self._services[key] = rospy.Service(values[0],
                                                values[1],
                                                self.handle_services)

        rospy.Subscriber("/experiment_state", UInt32, self.get_experiment_state)
        self._run_decoder = True

    def run(self):
        self._serial_port_adapter.run()

        decode_incoming_messages_thread = threading.Thread(target=self.decode_incoming_messages)
        decode_incoming_messages_thread.setDaemon(True)
        decode_incoming_messages_thread.start()

        decode_incoming_messages_thread = threading.Thread(target=self.downlink)
        decode_incoming_messages_thread.setDaemon(True)
        decode_incoming_messages_thread.start()


    def close(self):
        self._run_decoder = False
        self._serial_port_adapter.close()

    def run_log_decoder(self, filename):
        # while True:
        data = self._serial_port_adapter.decode_log(filename)
        for item in data:
            self.decode_frame(item)


    def decode_incoming_messages(self):
        # pub_bitrate = rospy.Publisher("/communication/downlink", Float64, queue_size=10)
        # bitrate = Float64()
        # self.timestamp_pub = rospy.Publisher('/ground_station/timestamp', Header, queue_size=1)
        # self.timestamp_msg = Header()

        while self._run_decoder:
            self.decode_frame(self._serial_port_adapter.receive())
            # bitrate.data = self._serial_port_adapter.get_bitrate()
            # pub_bitrate.publish(bitrate)

            # self.timestamp_msg.stamp = rospy.get_rostime()
            # self.timestamp_pub.publish(self.timestamp_msg)

            if self._serial_port_adapter.is_port_closed():
                self._logger.error(LOG_TAG,
                                   "RXSM is disconnected. Wait 10s and try connect again")
                time.sleep(10)
                self._serial_port_adapter.run()

    def downlink(self):
        pub_bitrate = rospy.Publisher("/communication/downlink", Float64, queue_size=10)
        bitrate = Float64()
        rate = rospy.Rate(1)
        while self._run_decoder:
            bitrate.data = self._serial_port_adapter.get_bitrate()
            pub_bitrate.publish(bitrate)
            rate.sleep()


    def decode_frame(self, frame):
        if frame != None:
            message_id = frame[0]
            if message_id in self._handlers_incoming.keys():
                message = frame[1:]
                self._handlers_incoming[message_id].decode(message)
            elif message_id in self._handlers_outgoing.keys():
                msg_ack = frame[2]
                self._logger.warning(LOG_TAG,
                                     "ACK received with code {:02x}".format(
                                         struct.unpack("B", msg_ack)[0]))
                self._command_list_actions[message_id] = msg_ack

            else:
                self._logger.error(LOG_TAG,
                                   "Message id is invalid")

    def encode_outgoing_messages(self, message_id, value):
        message = self._handlers_outgoing[message_id].encode(value)
        self._command_list_actions[message_id] = '\x00'
        frame_send = "{}{}{}".format(message_id,
                                     struct.pack("B", self._msg_send_cnt),
                                     message)
        self._serial_port_adapter.send(frame_send)
        self._msg_send_cnt += 1
        if self._msg_send_cnt > 255:
            self._msg_send_cnt = 0


    def handle_services(self, request):
        service_mode = tracz_actions_types[request.key][3]
        # # 1 - test mode, 2 - flight mode, 3 - both
        # first_bit - test mode , second_bit - flight mode

        if(self._experiment_state < 1 or self._experiment_state > 3):
            self._logger.warning(LOG_TAG, "Experiment state is undefine")
            return [False, 254]

        # if(self._experiment_state == 1):
        #     self._logger.warning(LOG_TAG, "Uplink is disabled in strict flight mode")
        #     return [False, 254]

        if(self._experiment_state == 3 and service_mode == 2):
            self._logger.warning(LOG_TAG, "Command is not available in test mode")
            return [False, 254]

        if(self._experiment_state == 2 and service_mode == 1):
            self._logger.warning(LOG_TAG, "Command is not available in flight mode")
            return [False, 254]

        if tracz_actions_types[request.key][1] == CommandListBool:
            self.encode_outgoing_messages(request.key, "")
            return self.wait_for_ack(request.key, timeout_in_sec=5)
        elif tracz_actions_types[request.key][1] == CommandListFloat:
            self.encode_outgoing_messages(request.key, request.value)
            return self.wait_for_ack(request.key, timeout_in_sec=5)
        else:
            self._logger.fatal(LOG_TAG, "Handle services fatal")


    def wait_for_ack(self, message_id, timeout_in_sec): #pylint: disable=too-many-return-statements
        dtime = 0.01
        max_value = int(timeout_in_sec/dtime)
        self._logger.warning(LOG_TAG,
                             "Command List Queue {}".format(
                                 [tracz_actions_types[key][2]
                                  for key in self._command_list_actions.iterkeys()]
                             ))
        for i in range(max_value): #pylint: disable=unused-variable
            if (self._command_list_actions[message_id] == '\x01'):
                self._command_list_actions.pop(message_id)
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK".format(
                                         tracz_actions_types[message_id][2]))
                return [True, 1]
            elif(self._command_list_actions[message_id] == '\x02'):
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK CRC ERROR ".format(
                                         [message_id][2]))
                self._command_list_actions.pop(message_id)
                return [False, 2]
            elif(self._command_list_actions[message_id] == '\x03'):
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK ID ERROR".format(
                                         tracz_actions_types[message_id][2]))
                self._command_list_actions.pop(message_id)
                return [False, 3]
            elif(self._command_list_actions[message_id] == '\x04'):
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK NANOPB ERROR".format(
                                         tracz_actions_types[message_id][2]))
                self._command_list_actions.pop(message_id)
                return [False, 4]
            elif(self._command_list_actions[message_id] == '\x05'):
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK CMD BUSY".format(
                                         tracz_actions_types[message_id][2]))
                self._command_list_actions.pop(message_id)
                return [False, 5]
            elif(self._command_list_actions[message_id] == '\x06'):
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK CMD EXEC FAILURE".format(
                                         tracz_actions_types[message_id][2]))
                self._command_list_actions.pop(message_id)
                return [False, 6]
            elif(self._command_list_actions[message_id] == '\x07'):
                self._logger.warning(LOG_TAG,
                                     "Command List \"{}\" ACK CMD NOT IMPLEMENTED".format(
                                         tracz_actions_types[message_id][2]))
                self._command_list_actions.pop(message_id)
                return [False, 7]
            else:
                time.sleep(dtime)
        self._logger.warning(LOG_TAG,
                             "Command List \"{}\" Timeout".format(
                                 tracz_actions_types[message_id][2]))
        self._command_list_actions.pop(message_id)
        return [False, 255]

    def get_experiment_state(self, data):
        # experiment_mode = 2 # Flight mode
        # experiment_mode = 3 # Test mode
        strict_flight = int((((data.data >> 1) & 1) == 0) and ((data.data >> 0) & 1))
        flight = int((((data.data >> 1) & 1) and (((data.data >> 0) & 1)) == 0))
        test = ((data.data >> 1) & 1) and ((data.data >> 0) & 1)

        # strict_flight = data.data & 0b1
        # flight = (data.data >> 1) & 0b1


        # self._logger.info(LOG_TAG, "Strict Flight mode {}".format(strict_flight))
        # self._logger.info(LOG_TAG, "Flight mode {}".format(flight))
        # self._logger.info(LOG_TAG, "Test mode {}".format(test))
        if(test == 1):
            self._experiment_state = 3
        elif(flight == 1):
            self._experiment_state = 2
        elif(strict_flight == 1):
            self._experiment_state = 1
