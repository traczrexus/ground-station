#!/usr/bin/env python

import sys
from core.LogAnalyzer import LogAnalyzer, plot_data_from_file


def node():
    if len(sys.argv)>1:
        filename = sys.argv[1]
        type = sys.argv[2]
        yaxis = sys.argv[3]

        log_analyzer = LogAnalyzer(filename)
        # log_analyzer.save_to_file(filename)
        plot_data_from_file(type+".dat", yaxis)


    else:
        print("Not valid parameters")



if __name__ == '__main__':
    node()
