#!/usr/bin/env python

import sys
from os import listdir
from os.path import isfile, join

import re
def sorted_nicely( l ):
    """ Sorts the given iterable in the way that is expected.

    Required arguments:
    l -- The iterable to be sorted.

    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)

def node():
    if len(sys.argv)>1:
        mypath = sys.argv[1]

        allfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        MF_FILES = []
        SF_FILES = []

        for file in allfiles:
            if "MF_" in file:
                MF_FILES.append(file)
            if "SF_" in file:
                SF_FILES.append(file)

        MF_FILES = sorted_nicely(MF_FILES)
        SF_FILES = sorted_nicely(SF_FILES)

        file_mother = open(mypath+"mf.dat","w")
        for file in MF_FILES:
            f = open(mypath+file, "r")
            file_mother.write(f.read())
            f.close()
        file_mother.close()

        file_mother = open(mypath+"sf.dat","w")
        for file in SF_FILES:
            f = open(mypath+file, "r")
            file_mother.write(f.read())
            f.close()
        file_mother.close()

        # print(MF_FILES)
        # print(SF_FILES)


    else:
        print("Not valid parameters")

if __name__ == '__main__':
    node()
