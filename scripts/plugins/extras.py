from python_qt_binding.QtGui import QPixmap, QPainter, QColor, QIcon #pylint: disable=no-name-in-module,import-error
from python_qt_binding.QtCore import Qt #pylint: disable=no-name-in-module,import-error

import rospkg
from tracz_types import tracz_modes, tracz_states, sd_card_error_code
# from core.Logger import Logger
# logger = Logger()

ROSPACK = rospkg.RosPack()
ACTUAL_PATH = ROSPACK.get_path('tracz_ground_station')

LOG_TAG = "pixelmap"

def draw_circle(color, size, border):
    pixel_map = QPixmap(size, size)
    pixel_map.fill(QColor(0, 0, 0, 0))

    painter = QPainter(pixel_map)
    painter.setPen(Qt.black)
    painter.setBrush(color)
    painter.drawEllipse(border,
                        border,
                        size-border,
                        size-border)
    painter.end()

    return pixel_map

def led_red():
    return QPixmap(ACTUAL_PATH+"/ui/img/led-red.png")

def led_green():
    return QPixmap(ACTUAL_PATH+"/ui/img/led-green.png")

def emergency_button(button):
    button.setIcon(QIcon(ACTUAL_PATH+"/ui/img/button.png"))

# GREEN_PIXELMAP = draw_circle(Qt.green, 30, 1.5)
# RED_PIXELMAP = draw_circle(Qt.red, 30, 1.5)

GREEN_PIXELMAP = led_green()
RED_PIXELMAP = led_red()

LABEL_INDICATORS = {}

def get_min_sec(sec):
    minutes, seconds = divmod(int(sec), 60)
    return '{:02.0f}:{:02.0f}'.format(minutes, seconds)

def label_circle(label, value):
    # value = int(value)
    # logger.warning(LOG_TAG, "keys {}".format(label_indicators.keys()))
    if label in LABEL_INDICATORS.keys():
        # logger.warning(LOG_TAG, "value {} value {}".format(label_indicators[label], value))
        if LABEL_INDICATORS[label] != value:
            LABEL_INDICATORS[label] = value
            update_label(label, value)
    else:
        LABEL_INDICATORS[label] = value
        # logger.warning(LOG_TAG, "label {}, value {}".format(label.pixmap, value))
        update_label(label, value)

def update_label(label, value):
    if value:
        label.setPixmap(GREEN_PIXELMAP)
    else:
        label.setPixmap(RED_PIXELMAP)

def print_response(response):
    response_str = "Error code not supported"
    if response == 0:
        response_str = ""
    elif response == 1:
        response_str = "ACK!"
    elif response == 2:
        response_str = "ACK CRC ERROR!"
    elif response == 3:
        response_str = "ACK ID ERROR!"
    elif response == 4:
        response_str = "ACK NANOPB ERROR!"
    elif response == 5:
        response_str = "ACK BUSY!"
    elif response == 6:
        response_str = "ACK EXEC FAILURE!"
    elif response == 7:
        response_str = "ACK NOT IMPLEMENTED!"
    elif response == 254:
        response_str = "Cannot send in this mode"
    elif response == 255:
        response_str = "Timeout!"

    return response_str

def print_experiment_mode(mode):
    response = "Mode undefine"
    if mode in tracz_modes:
        response = tracz_modes[mode]
    return response

def print_experiment_state(state):
    response = "State undefine"
    if state in tracz_states:
        response = tracz_states[state]
    return response

def print_sd_card_error(error_code):
    response = "Error sd card"
    if error_code in sd_card_error_code:
        response = sd_card_error_code[error_code]
    return response
