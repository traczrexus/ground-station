#!/usr/bin/env python

import os
import threading
import rospkg
# import tf
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from tracz_types import tracz_actions_types
from core.Logger import Logger

# from std_msgs.msg import Float64
# from std_msgs.msg import Bool

from plugins.CommandListHistory import add_command_to_history

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 15


class TestMode(Plugin): #pylint: disable=too-many-instance-attributes
    _logger = Logger()
    _services = {}
    _thread_is_running = True

    def __init__(self, context):
        super(TestMode, self).__init__(context)
        self.setObjectName('TestMode')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'test_mode.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('TestMode')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        for key, values in tracz_actions_types.iteritems():
            # rospy.wait_for_service(values[0])
            self._services[key] = rospy.ServiceProxy(values[0], values[1])

        self._widget.desired_pressure.clicked.connect(lambda: self.send_service('\x1c'))
        self._widget.desired_position.clicked.connect(lambda: self.send_service('\x12'))
        self._widget.single_grip.clicked.connect(lambda: self.send_service('\x63'))
        self._widget.full_sequence.clicked.connect(lambda: self.send_service('\x64'))


    def send_service(self, key):
        update_rqt = threading.Thread(target=self.send_service_thread, args=(key,))
        update_rqt.start()

    def send_service_thread(self, key):
        try:
            if key == '\x1c':
                response = self._services[key](key, self._widget.diff_pressure_demand_value.value())
            elif key == '\x12':
                response = self._services[key](key, self._widget.move_motor_position.value())
            else:
                response = self._services[key](key)

            add_command_to_history(tracz_actions_types[key][2], response.ack_code)
        except rospy.ServiceException as exception:
            self._logger.error(LOG_TAG, "SERVICE ERROR: {}".format(exception))



    def shutdown_plugin(self):
        self._thread_is_running = False
