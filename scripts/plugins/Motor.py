#!/usr/bin/env python

import os
import threading
import rospkg
import tf
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from tracz_types import tracz_actions_types

from std_msgs.msg import Float64
from std_msgs.msg import Bool

from plugins.extras import update_label
from plugins.CommandListHistory import add_command_to_history

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 15


class Motor(Plugin): #pylint: disable=too-many-instance-attributes
    _services = {}
    _subscribers = []
    _home_position = True
    _load_cell = 0
    _motor_position = 0
    _motor_velocity = 0
    _motor_current = 0
    _electrolock = True

    _thread_is_running = True

    def __init__(self, context):
        super(Motor, self).__init__(context)
        self.setObjectName('Motor')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'motor.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('TraczMotor')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        for key, values in tracz_actions_types.iteritems():
            # rospy.wait_for_service(values[0])
            self._services[key] = rospy.ServiceProxy(values[0], values[1])

        self._subscribers.append(rospy.Subscriber("/gripper/home",
                                                  Bool,
                                                  self.home_position))
        self._subscribers.append(rospy.Subscriber("/electrolock",
                                                  Bool,
                                                  self.electrolock))
        self._subscribers.append(rospy.Subscriber("/load_cell/force",
                                                  Float64,
                                                  self.load_cell_force))
        self._subscribers.append(rospy.Subscriber("/motor/current",
                                                  Float64,
                                                  self.motor_current))
        self._subscribers.append(rospy.Subscriber("/motor/position/actual",
                                                  Float64,
                                                  self.motor_position))
        self._subscribers.append(rospy.Subscriber("/motor/velocity/actual",
                                                  Float64,
                                                  self.motor_velocity))

        self._widget.lock.clicked.connect(lambda: self.send_service('\x77'))
        self._widget.unlock.clicked.connect(lambda: self.send_service('\x76'))
        self._widget.move_to_home_position.clicked.connect(lambda: self.send_service('\x66'))
        self._widget.move_to_sample_position.clicked.connect(lambda: self.send_service('\x65'))

        update_label(self._widget.home_position, self._home_position)
        update_label(self._widget.electrolock, self._electrolock)

        update_rqt = threading.Thread(target=self.update)
        update_rqt.setDaemon(True)
        update_rqt.start()

    def update(self):
        transform_broadcaster = tf.TransformBroadcaster()
        transform_broadcaster.sendTransform((-0.025, 0, 0),
                                            (0, 0, 0, 1),
                                            rospy.get_rostime(),
                                            "foot",
                                            "base_link")

        rate = rospy.Rate(REFRESH_RATE)
        while self._thread_is_running:
            self._widget.load_cell_force.setText("{:.1f} N".format(self._load_cell))
            self._widget.motor_current.setText("{:.2f} A".format(self._motor_current))
            self._widget.motor_position.setText("{:.2f} mm".format(self._motor_position))
            self._widget.motor_velocity.setText("{:.2f} mm/s".format(self._motor_velocity))
            rate.sleep()

    def send_service(self, key):
        update_rqt = threading.Thread(target=self.send_service_thread, args=(key,))
        update_rqt.start()

    def send_service_thread(self, key):
        try:
            if key == "\x12":
                response = self._services[key](key, self._widget.move_motor_position.value())
            else:
                response = self._services[key](key)

            add_command_to_history(tracz_actions_types[key][2], response.ack_code)
        except rospy.ServiceException as exception:
            self._logger.error(LOG_TAG, "SERVICE ERROR: {}".format(exception))

    def shutdown_plugin(self):
        self._thread_is_running = False
        for item in self._subscribers:
            item.unregister()

    def home_position(self, data):
        if self._home_position != data.data:
            self._home_position = data.data
            update_label(self._widget.home_position, self._home_position)

    def electrolock(self, data):
        if self._electrolock != data.data:
            self._electrolock = data.data
            update_label(self._widget.electrolock, self._electrolock)


    def load_cell_force(self, data):
        self._load_cell = 0.1 * data.data + 0.9* self._load_cell
        # self._load_cell = data.data

    def motor_current(self, data):
        self._motor_current = data.data

    def motor_position(self, data):
        self._motor_position = data.data

    def motor_velocity(self, data):
        self._motor_velocity = data.data
