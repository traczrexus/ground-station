#!/usr/bin/env python

import os
import threading
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from tracz_types import tracz_actions_types

from std_msgs.msg import Float64
from std_msgs.msg import Bool

from plugins.extras import update_label
from plugins.CommandListHistory import add_command_to_history

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 15


class Pneumatics(Plugin):
    _subscribers = []
    _services = {}
    _diff_pressure = 0
    _valve_ambient = False
    _valve_tank = False

    _thread_is_running = True

    def __init__(self, context):
        super(Pneumatics, self).__init__(context)
        self.setObjectName('Pneumatics')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'tracz_pneumatics.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('TraczPneumatics')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        _commands = ['\x5B', '\x5C']
        _allowed_commands = {x:tracz_actions_types[x] for x in _commands}
        for key, values in _allowed_commands.iteritems():
            # rospy.wait_for_service(values[0])
            self._services[key] = rospy.ServiceProxy(values[0], values[1])

        # Valves
        # self._widget.valveTankOnButton.clicked.connect(lambda: self.send_service('\x6F'))
        # self._widget.valveTankOffButton.clicked.connect(lambda: self.send_service('\x70'))
        # self._widget.valveAmbientOnButton.clicked.connect(lambda: self.send_service('\x71'))
        # self._widget.valveAmbientOffButton.clicked.connect(lambda: self.send_service('\x72'))
        # self._widget.valves_close.clicked.connect(lambda: self.send_service('\x73'))
        # self._widget.diff_pressure_demand_send.clicked.connect(lambda: self.send_service('\x1c'))

        self._subscribers.append(rospy.Subscriber("/pneumatics/differential_pressure",
                                                  Float64,
                                                  self.clb_diff_pressure))
        self._subscribers.append(rospy.Subscriber("/pneumatics/valve/ambient/state",
                                                  Bool,
                                                  self.clb_ambient))
        self._subscribers.append(rospy.Subscriber("/pneumatics/valve/tank/state",
                                                  Bool,
                                                  self.clb_tank))

        self._widget.button_gas_on.clicked.connect(lambda: self.send_service('\x5B'))
        self._widget.button_gas_off.clicked.connect(lambda: self.send_service('\x5C'))

        update_label(self._widget.valve_ambient_value, self._valve_ambient)
        update_label(self._widget.valve_tank_value, self._valve_tank)

        update_rqt = threading.Thread(target=self.update)
        update_rqt.setDaemon(True)
        update_rqt.start()

    def update(self):
        rate = rospy.Rate(REFRESH_RATE)
        while self._thread_is_running:
            self._widget.diff_pressure_value.setText("{:.1f} kPa".format(self._diff_pressure))
            rate.sleep()

    def shutdown_plugin(self):
        self._thread_is_running = False
        for item in self._subscribers:
            item.unregister()
        # for key in self._services.iterkeys():
        #     self._services[key].close()

    def clb_diff_pressure(self, data):
        self._diff_pressure = data.data

    def clb_ambient(self, data):
        if self._valve_ambient != data.data:
            self._valve_ambient = data.data
            update_label(self._widget.valve_ambient_value, self._valve_ambient)

    def clb_tank(self, data):
        if self._valve_tank != data.data:
            self._valve_tank = data.data
            update_label(self._widget.valve_tank_value, self._valve_tank)

    def send_service(self, key):
        update_rqt = threading.Thread(target=self.send_service_thread, args=(key,))
        update_rqt.start()

    def send_service_thread(self, key):
        # if key == "\x1c":
        #     response = self._services[key](key, self._widget.diff_pressure_demand_value.value())
        # else:
        #     response = self._services[key](key)
        response = self._services[key](key)

        add_command_to_history(tracz_actions_types[key][2], response.ack_code)
