#!/usr/bin/env python

import os
import threading
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from tracz_types import tracz_actions_types
from tracz_ground_station.srv import CommandListBool, CommandListFloat
from plugins.CommandListHistory import add_command_to_history

from core.Logger import Logger

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 15


class CommandList(Plugin):
    _services = {}
    _logger = Logger()
    _excluded_commands = ['\x6f', '\x70', '\x71', '\x72', '\x73', '\x76', '\x77',
                          '\x65', '\x66', '\x52', '\x53', '\x54', '\x55', '\x56',
                          '\x57', '\x1c', '\x52', '\x53', '\x54', '\x55', '\x56',
                          '\x57', '\x58', '\x59', '\x5A', '\x74', '\x75', '\x12',
                          '\x61', '\x62', '\x63', '\x50', '\x51']

    def __init__(self, context):
        super(CommandList, self).__init__(context)
        self.setObjectName('CommandList')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'command_list.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('CommandList')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(
                "Command List" + (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)


        for key, values in tracz_actions_types.iteritems():
            if key not in self._excluded_commands:
                # rospy.wait_for_service(values[0])
                self._services[key] = rospy.ServiceProxy(values[0], values[1])
                if(values[1] == CommandListBool):
                    self._widget.empty_friendly_name.addItem(values[2])
                else:
                    self._widget.float_friendly_name.addItem(values[2])

        self._widget.float_send.clicked.connect(lambda: self.send_action(CommandListFloat))
        self.response_print(CommandListBool, 0)

        self._widget.empty_send.clicked.connect(lambda: self.send_action(CommandListBool))
        self.response_print(CommandListFloat, 0)

    def send_action(self, command_type):
        if command_type == CommandListFloat:
            self._widget.float_send.setEnabled(False)
            name = str(self._widget.float_friendly_name.currentText())
            for key, value in tracz_actions_types.iteritems():
                if value[2] == name:
                    self.send_service(key)
            self._widget.float_send.setEnabled(True)
        if command_type == CommandListBool:
            self._widget.empty_send.setEnabled(False)
            name = str(self._widget.empty_friendly_name.currentText())
            for key, value in tracz_actions_types.iteritems():
                if value[2] == name:
                    self.send_service(key)
            self._widget.empty_send.setEnabled(True)

    def send_service(self, key):
        update_rqt = threading.Thread(target=self.send_service_thread, args=(key,))
        update_rqt.start()

    def send_service_thread(self, key):
        try:
            if tracz_actions_types[key][1] == CommandListBool:
                self.response_print(CommandListBool, 0)
                response = self._services[key](key)
                self.response_print(CommandListBool, response.ack_code)
            else:
                self.response_print(CommandListFloat, 0)
                response = self._services[key](key, self._widget.float_value.value())
                self.response_print(CommandListFloat, response.ack_code)

                add_command_to_history(tracz_actions_types[key][2], response.ack_code)
        except rospy.ServiceException as exception:
            self._logger.error(LOG_TAG, "SERVICE ERROR: {}".format(exception))

    def response_print(self, command_type, response):
        response_str = ""
        if response == 0:
            response_str = "Response:"
        if response == 1:
            response_str = "Response: ACK!"
        if response == 2:
            response_str = "Response: ACK CRC ERROR!"
        if response == 3:
            response_str = "Response: ACK ID ERROR!"
        if response == 4:
            response_str = "Response: ACK NANOPB ERROR!"
        if response == 5:
            response_str = "Response: ACK BUSY!"
        if response == 6:
            response_str = "Response: ACK EXEC FAILURE!"
        if response == 7:
            response_str = "Response: ACK NOT IMPLEMENTED!"
        if response == 255:
            response_str = "Response: Timeout!"

        if command_type == CommandListBool:
            self._widget.empty_response.setText(response_str)
        if command_type == CommandListFloat:
            self._widget.float_response.setText(response_str)
