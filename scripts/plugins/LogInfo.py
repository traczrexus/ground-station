#!/usr/bin/env python

import os
# import threading
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from std_msgs.msg import Bool, UInt32

from plugins.extras import update_label

REFRESH_RATE = 10

class LogInfo(Plugin): #pylint: disable=too-many-instance-attributes
    _subscribers = []
    # _thread_is_running = True

    _mb_data_logging = 0
    _mb_sd = 0
    _mb_card_detect = 0
    _sb_data_logging = 0
    _sb_card_detect = 0
    _sb_sd = 0
    _state_12v = 0
    _gas_releasing = 0

    def __init__(self, context):
        super(LogInfo, self).__init__(context)
        self.setObjectName('LogInfo')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'log_info.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('LogInfo')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        self._subscribers.append(rospy.Subscriber("/mainboard/data_logging",
                                                  Bool,
                                                  self.mb_data_logging))
        self._subscribers.append(rospy.Subscriber("/mainboard/sd_card",
                                                  Bool,
                                                  self.mb_sd))
        self._subscribers.append(rospy.Subscriber("/sensorboard/data_logging",
                                                  Bool,
                                                  self.sb_data_logging))
        self._subscribers.append(rospy.Subscriber("/sensorboard/sd_card",
                                                  Bool,
                                                  self.sb_sd))
        self._subscribers.append(rospy.Subscriber("/info/12v_state",
                                                  Bool,
                                                  self.state_12v))
        self._subscribers.append(rospy.Subscriber("/experiment_state",
                                                  UInt32,
                                                  self.experiment_state))
        self._subscribers.append(rospy.Subscriber("/mainboard/error_info",
                                                  UInt32,
                                                  self.mb_error))
        self._subscribers.append(rospy.Subscriber("/sensorboard/error_info",
                                                  UInt32,
                                                  self.sb_error))



    #     update_rqt = threading.Thread(target=self.update)
    #     update_rqt.setDaemon(True)
    #     update_rqt.start()
    #
    # def update(self):
    #     rate = rospy.Rate(REFRESH_RATE)
    #     while self._thread_is_running:
    #         update_label(self._widget.sb_data_logging, self._sb_data_logging)
    #         update_label(self._widget.sb_sd_card, self._sb_sd)
    #         rate.sleep()
        update_label(self._widget.state_12v, self._state_12v)
        update_label(self._widget.mb_data_logging, self._mb_data_logging)
        update_label(self._widget.mb_sd_card, self._mb_sd)
        update_label(self._widget.sb_data_logging, self._sb_data_logging)
        update_label(self._widget.sb_sd_card, self._sb_sd)
        update_label(self._widget.gas_releasing, self._gas_releasing)
        update_label(self._widget.card_detect_mb, self._mb_card_detect)
        update_label(self._widget.card_detect_sb, self._sb_card_detect)


    def shutdown_plugin(self):
        # self._thread_is_running = False
        for item in self._subscribers:
            item.unregister()

    def state_12v(self, data):
        if self._state_12v != data.data:
            self._state_12v = data.data
            update_label(self._widget.state_12v, self._state_12v)

    def mb_data_logging(self, data):
        if self._mb_data_logging != data.data:
            self._mb_data_logging = data.data
            update_label(self._widget.mb_data_logging, self._mb_data_logging)

    def mb_sd(self, data):
        if self._mb_sd != data.data:
            self._mb_sd = data.data
            update_label(self._widget.mb_sd_card, self._mb_sd)

    def sb_data_logging(self, data):
        if self._sb_data_logging != data.data:
            self._sb_data_logging = data.data
            update_label(self._widget.sb_data_logging, self._sb_data_logging)

    def sb_sd(self, data):
        if self._sb_sd != data.data:
            self._sb_sd = data.data
            update_label(self._widget.sb_sd_card, self._sb_sd)

    def experiment_state(self, data):
        if self._gas_releasing != ((data.data >> 19) & 0b1):
            self._gas_releasing = ((data.data >> 19) & 0b1)
            update_label(self._widget.gas_releasing, self._gas_releasing)

    def mb_error(self, data):
        if self._mb_card_detect != (data.data & 0b1):
            self._mb_card_detect = data.data & 0b1
            update_label(self._widget.card_detect_mb, self._mb_card_detect)

    def sb_error(self, data):
        if self._sb_card_detect != (data.data & 0b1):
            self._sb_card_detect = data.data & 0b1
            update_label(self._widget.card_detect_sb, self._sb_card_detect)
