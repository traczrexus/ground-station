#!/usr/bin/env python

import os
import threading
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget, QTableWidgetItem #pylint: disable=no-name-in-module,import-error

from std_msgs.msg import Float64
# from tracz_ground_station.srv import CommandListBool, CommandListFloat
from plugins.extras import get_min_sec, print_response

# from core.Logger import Logger
# logger = Logger()

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 1


def add_command_to_history(key, ack):
    # logger.warning(LOG_TAG, "Add to list {} {}".format(key, ack))
    COMMANDS_HISTORY.append([key, ack])

COMMANDS_HISTORY = []


class CommandListHistory(Plugin):
    _thread_is_running = True
    _history_len = len(COMMANDS_HISTORY)
    _timestamp = 0.0

    def __init__(self, context):
        super(CommandListHistory, self).__init__(context)
        self.setObjectName('CommandListHistory')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'command_list_history.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('CommandListHistory')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(
                "CommandListHistory" + (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        rospy.Subscriber("/mainboard/timestamp", Float64, self.timestamp)

        update_rqt = threading.Thread(target=self.update)
        update_rqt.setDaemon(True)
        update_rqt.start()

    def update(self):
        rate = rospy.Rate(REFRESH_RATE)
        self._history_len = len(COMMANDS_HISTORY)
        while self._thread_is_running:
            while self._history_len < len(COMMANDS_HISTORY):
                self.add_to_table(self._timestamp,
                                  COMMANDS_HISTORY[self._history_len][0],
                                  COMMANDS_HISTORY[self._history_len][1])
                self._history_len += 1
            rate.sleep()

    def shutdown_plugin(self):
        self._thread_is_running = False

    def add_to_table(self, timestamp, key, ack):
        # logger.warning(LOG_TAG, "key {}".format(tracz_actions_types[key][2]))
        self._widget.tableWidget.insertRow(0)
        self._widget.tableWidget.setItem(0, 0, QTableWidgetItem(get_min_sec(timestamp)))
        self._widget.tableWidget.setItem(0, 1, QTableWidgetItem(key))
        self._widget.tableWidget.setItem(0, 2, QTableWidgetItem(print_response(ack)))

    def timestamp(self, data):
        self._timestamp = data.data
