#!/usr/bin/env python

import os
import threading
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from tracz_types import tracz_actions_types
from plugins.CommandListHistory import add_command_to_history
from plugins.extras import print_experiment_mode, print_experiment_state, \
                           emergency_button, print_sd_card_error
from std_msgs.msg import UInt32


from core.Logger import Logger

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 15

class Control(Plugin):
    _services = {}
    _subscribers = []
    # _thread_is_running = True
    _logger = Logger()

    _experiment_state = 0
    _mode = 0
    _state = 0
    _sb_error_info = 0
    _mb_error_info = 0
    _emergency_stop = 0

    def __init__(self, context):
        super(Control, self).__init__(context)
        self.setObjectName('Control')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'tracz_control.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('Control')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        _commands = ['\x61', '\x62', '\x50', '\x51']
        _allowed_commands = {x:tracz_actions_types[x] for x in _commands}
        for key, values in _allowed_commands.iteritems():
            # rospy.wait_for_service(values[0])
            self._services[key] = rospy.ServiceProxy(values[0], values[1])

        self._subscribers.append(rospy.Subscriber("/experiment_state",
                                                  UInt32,
                                                  self.experiment_state))
        self._subscribers.append(rospy.Subscriber("/mainboard/error_info",
                                                  UInt32,
                                                  self.mb_error_info))
        self._subscribers.append(rospy.Subscriber("/sensorboard/error_info",
                                                  UInt32,
                                                  self.sb_error_info))

        emergency_button(self._widget.emengercy_stop)


        self._widget.emengercy_stop.clicked.connect(lambda: self.send_service('\x61'))
        self._widget.unlock_emengercy.clicked.connect(lambda: self.send_service('\x62'))
        self._widget.flight_mode.clicked.connect(lambda: self.send_service('\x50'))
        self._widget.test_mode.clicked.connect(lambda: self.send_service('\x51'))

    #     update_rqt = threading.Thread(target=self.update)
    #     update_rqt.setDaemon(True)
    #     update_rqt.start()
    #
    # def update(self):
    #     rate = rospy.Rate(REFRESH_RATE)
    #     while self._thread_is_running:
    #
    #         # self._widget.sb_error_info.setText("{}".format(self._sb_error_info))
    #         rate.sleep()

    def send_service(self, key):
        update_rqt = threading.Thread(target=self.send_service_thread, args=(key,))
        update_rqt.start()

    def send_service_thread(self, key):
        try:
            response = self._services[key](key)
            add_command_to_history(tracz_actions_types[key][2], response.ack_code)
        except rospy.ServiceException as exception:
            self._logger.error(LOG_TAG, "SERVICE ERROR: {}".format(exception))

    def shutdown_plugin(self):
        # self._thread_is_running = False
        for item in self._subscribers:
            item.unregister()

    def experiment_state(self, data):
        if self._experiment_state != data.data:
            self._experiment_state = data.data
            self._mode = self._experiment_state & 0b11
            self._state = (self._experiment_state >> 6) & 0b1111111111
            self._emergency_stop = (self._experiment_state >> 2) & 0b1
            add_command_to_history(print_experiment_state(self._state), 0)
            if self._mode == 2:
                self._widget.test_mode.setStyleSheet("background-color: grey")
                self._widget.flight_mode.setStyleSheet("background-color: green")
                self._widget.test_mode.setEnabled(True)
                self._widget.flight_mode.setEnabled(True)
                self.set_emergency()
#                self._widget.emengercy_stop.setEnabled(True)
#                self._widget.unlock_emengercy.setEnabled(True)
            elif self._mode == 3:
                self._widget.test_mode.setStyleSheet("background-color: green")
                self._widget.flight_mode.setStyleSheet("background-color: grey")
                self._widget.test_mode.setEnabled(True)
                self._widget.flight_mode.setEnabled(True)
                self.set_emergency()
#                self._widget.emengercy_stop.setEnabled(True)
#                self._widget.unlock_emengercy.setEnabled(True)
            else:
                self._widget.test_mode.setEnabled(False)
                self._widget.flight_mode.setEnabled(False)
                self._widget.emengercy_stop.setEnabled(False)
                self._widget.unlock_emengercy.setEnabled(False)
                self._widget.test_mode.setStyleSheet("background-color: grey")
                self._widget.flight_mode.setStyleSheet("background-color: grey")

            self._widget.experiment_mode.setText("{}".format(
                print_experiment_mode(self._mode)))
            self._widget.experiment_state.setText("{}".format(
                print_experiment_state(self._state)))

    def set_emergency(self):
        if self._emergency_stop:
            self._widget.emengercy_stop.setEnabled(False)
            self._widget.unlock_emengercy.setEnabled(True)
            self._widget.unlock_emengercy.setStyleSheet("background-color: green")
        else:
            self._widget.emengercy_stop.setEnabled(True)
            self._widget.unlock_emengercy.setEnabled(False)
            self._widget.unlock_emengercy.setStyleSheet("background-color: grey")

    def sb_error_info(self, data):
        if self._sb_error_info != data.data:
            self._sb_error_info = data.data
            ff_mount = (self._sb_error_info >> 1) & 0b11111
            ff_mkdir = (self._sb_error_info >> 6) & 0b11111
            ff_open = (self._sb_error_info >> 11) & 0b11111
            ff_write = (self._sb_error_info >> 16) & 0b11111
            ff_close = (self._sb_error_info >> 21) & 0b11111

            error_str = "sensorboard error: write {}, open {}, mkdir {}, mount {}, close {}".format(
                print_sd_card_error(ff_write),
                print_sd_card_error(ff_open),
                print_sd_card_error(ff_mkdir),
                print_sd_card_error(ff_mount),
                print_sd_card_error(ff_close))
            self._logger.warning(LOG_TAG, error_str)
            add_command_to_history(error_str, 0)
            # self._widget.sb_error_info.setText("Error {} detected".format(self._sb_error_info))

    def mb_error_info(self, data):
        if self._mb_error_info != data.data:
            self._mb_error_info = data.data
            ff_mount = (self._mb_error_info >> 1) & 0b11111
            ff_mkdir = (self._mb_error_info >> 6) & 0b11111
            ff_open = (self._mb_error_info >> 11) & 0b11111
            ff_write = (self._mb_error_info >> 16) & 0b11111
            ff_close = (self._mb_error_info >> 21) & 0b11111

            error_str = "mainboard error: write {}, open {}, mkdir {}, mount {}, close {}".format(
                print_sd_card_error(ff_write),
                print_sd_card_error(ff_open),
                print_sd_card_error(ff_mkdir),
                print_sd_card_error(ff_mount),
                print_sd_card_error(ff_close))
            self._logger.warning(LOG_TAG, error_str)
            add_command_to_history(error_str, 0)
            # self._widget.mb_error_info.setText("Error {} detected".format(self._mb_error_info))
