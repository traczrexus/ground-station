#!/usr/bin/env python

import os
import threading
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from std_msgs.msg import Float64
from std_msgs.msg import Bool

from plugins.extras import update_label, get_min_sec
from plugins.CommandListHistory import add_command_to_history

from core.Logger import Logger

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 10

class RXSM(Plugin): #pylint: disable=too-many-instance-attributes
    _logger = Logger()
    _subscribers = []
    _sods = False
    _lo = False
    _soe = False
    _sods_sb = False
    _timestamp_mainboard = 0.0
    _timestamp_sensorboard = 0.0
    _timestamp_info = 0.0
    _thread_is_running = True
    _bitrate = 0.0
    _bitrate_status = False

    def __init__(self, context):
        super(RXSM, self).__init__(context)
        self.setObjectName('RXSM')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'tracz_rxsm.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('TraczRXSM')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        self._subscribers.append(rospy.Subscriber('/rxsm/sods', Bool, self.clb_sods))
        self._subscribers.append(rospy.Subscriber('/rxsm/lo', Bool, self.clb_lo))
        self._subscribers.append(rospy.Subscriber('/rxsm/soe', Bool, self.clb_soe))
        self._subscribers.append(rospy.Subscriber('/sensorboard/sods', Bool,
                                                  self.clb_sods_sb))
        self._subscribers.append(rospy.Subscriber('/mainboard/timestamp', Float64,
                                                  self.clb_timestamp_mb))
        self._subscribers.append(rospy.Subscriber('/sensorboard/timestamp', Float64,
                                                  self.clb_timestamp_sb))
        self._subscribers.append(rospy.Subscriber('/info/timestamp', Float64,
                                                  self.clb_timestamp_info))
        self._subscribers.append(rospy.Subscriber('/communication/downlink', Float64,
                                                  self.clb_bitrate))

        update_label(self._widget.sods_value, self._sods)
        update_label(self._widget.lo_value, self._lo)
        update_label(self._widget.soe_value, self._soe)
        update_label(self._widget.sods_sb_value, self._sods_sb)
        update_label(self._widget.bitrate_status, self._bitrate_status)

        update_rqt = threading.Thread(target=self.update)
        update_rqt.setDaemon(True)
        update_rqt.start()

    def update(self):
        rate = rospy.Rate(REFRESH_RATE)
        while self._thread_is_running:
            self._widget.timestamp_mainboard.setText(get_min_sec(self._timestamp_mainboard))
            self._widget.timestamp_sensorboard.setText(get_min_sec(self._timestamp_sensorboard))
            self._widget.timestamp_info.setText(get_min_sec(self._timestamp_info))
            rate.sleep()

    def shutdown_plugin(self):
        self._thread_is_running = False
        for item in self._subscribers:
            item.unregister()

    def clb_sods(self, data):
        if self._sods != data.data:
            self._sods = data.data
            update_label(self._widget.sods_value, self._sods)
            if self._sods:
                add_command_to_history("SODS ON", 0)
            else:
                add_command_to_history("SODS OFF", 0)

    def clb_lo(self, data):
        if self._lo != data.data:
            self._lo = data.data
            update_label(self._widget.lo_value, self._lo)
            if self._lo:
                add_command_to_history("LO ON", 0)
            else:
                add_command_to_history("LO OFF", 0)

    def clb_soe(self, data):
        if self._soe != data.data:
            self._soe = data.data
            update_label(self._widget.soe_value, self._soe)
            if self._soe:
                add_command_to_history("SOE ON", 0)
            else:
                add_command_to_history("SOE OFF", 0)

    def clb_sods_sb(self, data):
        if self._sods_sb != data.data:
            self._sods_sb = data.data
            update_label(self._widget.sods_sb_value, self._sods_sb)

    def clb_timestamp_mb(self, data):
        self._timestamp_mainboard = data.data

    def clb_timestamp_sb(self, data):
        self._timestamp_sensorboard = data.data

    def clb_timestamp_info(self, data):
        self._timestamp_info = data.data

    def clb_bitrate(self, data):
        if self._bitrate != data.data:
            self._bitrate = data.data
            self._widget.bitrate.setText("{:.3f} kbit/s".format(self._bitrate/1000))
            if self._bitrate > 27000 and self._bitrate < 30000:
                self._bitrate_status = True
            else:
                self._bitrate_status = False
            update_label(self._widget.bitrate_status, self._bitrate_status)
