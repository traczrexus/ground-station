#!/usr/bin/env python

import os
import threading
import time
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget #pylint: disable=no-name-in-module,import-error

from tracz_types import tracz_actions_types

from std_msgs.msg import Float64, UInt32
from plugins.CommandListHistory import add_command_to_history
from core.Logger import Logger

LOG_TAG = "tracz_plugins"
REFRESH_RATE = 15

class Sensorboard(Plugin):
    _services = {}
    _subscribers = []
    _acceleration = [0, 0, 0]
    _angular_velocity = [0, 0, 0]
    _pressure = 0
    _temperature = 0
    _humidity = 0
    _commands = ['\x52', '\x53', '\x54', '\x55', '\x56', '\x57', '\x5a', '\x74',
                 '\x75', '\x58', '\x59']
    _allowed_commands = {x:tracz_actions_types[x] for x in _commands}
    _experiment_state = 0

    _thread_is_running = True
    _logger = Logger()
    def __init__(self, context):
        super(Sensorboard, self).__init__(context)
        self.setObjectName('Sensorboard')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_ground_station'),
                               'ui',
                               'sensorboard.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('Sensorboard')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() +
                                        (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        for key, values in self._allowed_commands.iteritems():
            # rospy.wait_for_service(values[0])
            self._services[key] = rospy.ServiceProxy(values[0], values[1])

        self._subscribers.append(rospy.Subscriber("/acceleration/x",
                                                  Float64,
                                                  self.acc_x))
        self._subscribers.append(rospy.Subscriber("/acceleration/y",
                                                  Float64,
                                                  self.acc_y))
        self._subscribers.append(rospy.Subscriber("/acceleration/z",
                                                  Float64,
                                                  self.acc_z))
        self._subscribers.append(rospy.Subscriber("/angular_velocity/x",
                                                  Float64,
                                                  self.av_x))
        self._subscribers.append(rospy.Subscriber("/angular_velocity/y",
                                                  Float64,
                                                  self.av_y))
        self._subscribers.append(rospy.Subscriber("/angular_velocity/z",
                                                  Float64,
                                                  self.av_z))
        self._subscribers.append(rospy.Subscriber("/environment/absolute_pressure",
                                                  Float64,
                                                  self.pressure))
        self._subscribers.append(rospy.Subscriber("/environment/temperature",
                                                  Float64,
                                                  self.temperature))
        self._subscribers.append(rospy.Subscriber("/environment/humidity",
                                                  Float64,
                                                  self.humidity))

        self._subscribers.append(rospy.Subscriber("/experiment_state",
                                                  UInt32,
                                                  self.experiment_state))

        self._widget.camera1_on.clicked.connect(lambda: self.send_service('\x52'))
        self._widget.camera1_off.clicked.connect(lambda: self.send_service('\x53'))
        self._widget.camera2_on.clicked.connect(lambda: self.send_service('\x54'))
        self._widget.camera2_off.clicked.connect(lambda: self.send_service('\x55'))
        self._widget.camera3_on.clicked.connect(lambda: self.send_service('\x56'))
        self._widget.camera3_off.clicked.connect(lambda: self.send_service('\x57'))
        self._widget.tv1.clicked.connect(lambda: self.send_service('\x58'))
        self._widget.tv2.clicked.connect(lambda: self.send_service('\x59'))
        self._widget.tv3.clicked.connect(lambda: self.send_service('\x5A'))
        self._widget.ilumination_on.clicked.connect(lambda: self.send_service('\x74'))
        self._widget.ilumination_off.clicked.connect(lambda: self.send_service('\x75'))


        update_rqt = threading.Thread(target=self.update)
        update_rqt.setDaemon(True)
        update_rqt.start()

    def update(self):
        rate = rospy.Rate(REFRESH_RATE)
        while self._thread_is_running:
            self._widget.acceleration_x.setText("{:.1f} g".format(self._acceleration[0]))
            self._widget.acceleration_y.setText("{:.1f} g".format(self._acceleration[1]))
            self._widget.acceleration_z.setText("{:.1f} g".format(self._acceleration[2]))
            self._widget.angular_velocity_x.setText("{:.1f} dps".format(self._angular_velocity[0]))
            self._widget.angular_velocity_y.setText("{:.1f} dps".format(self._angular_velocity[1]))
            self._widget.angular_velocity_z.setText("{:.1f} dps".format(self._angular_velocity[2]))
            self._widget.pressure.setText("{:.1f} kPa".format(self._pressure))
            self._widget.temperature.setText(u"{:.1f} \u00b0C".format(self._temperature))
            self._widget.humidity.setText("{:.1f} %".format(self._humidity))
            rate.sleep()

    def send_service(self, key):
        update_rqt = threading.Thread(target=self.send_service_thread, args=(key,))
        update_rqt.start()

    def send_service_thread(self, key):
        try:
            response = self._services[key](key)
            add_command_to_history(tracz_actions_types[key][2], response.ack_code)
        except rospy.ServiceException as exception:
            self._logger.error(LOG_TAG, "SERVICE ERROR: {}".format(exception))

    def shutdown_plugin(self):
        self._thread_is_running = False
        for item in self._subscribers:
            item.unregister()

    def acc_x(self, data):
        self._acceleration[0] = data.data

    def acc_y(self, data):
        self._acceleration[1] = data.data

    def acc_z(self, data):
        self._acceleration[2] = data.data

    def av_x(self, data):
        self._angular_velocity[0] = data.data

    def av_y(self, data):
        self._angular_velocity[1] = data.data

    def av_z(self, data):
        self._angular_velocity[2] = data.data

    def pressure(self, data):
        self._pressure = data.data

    def temperature(self, data):
        self._temperature = data.data

    def humidity(self, data):
        self._humidity = data.data

    def experiment_state(self, data):
        if self._experiment_state != data.data:
            self._experiment_state = data.data
            mobius1 = (self._experiment_state >> 3) & 0b1
            mobius2 = (self._experiment_state >> 4) & 0b1
            mobius3 = (self._experiment_state >> 5) & 0b1
            illumination = (self._experiment_state >> 16) & 0b1
            tv_channel = (self._experiment_state >> 17) & 0b11
            time.sleep(0.1)
            if mobius1:
                self._widget.camera1_on.setStyleSheet("background-color: green")
                self._widget.camera1_off.setStyleSheet("background-color: grey")
            else:
                self._widget.camera1_off.setStyleSheet("background-color: green")
                self._widget.camera1_on.setStyleSheet("background-color: grey")

            if mobius2:
                self._widget.camera2_on.setStyleSheet("background-color: green")
                self._widget.camera2_off.setStyleSheet("background-color: grey")
            else:
                self._widget.camera2_off.setStyleSheet("background-color: green")
                self._widget.camera2_on.setStyleSheet("background-color: grey")

            if mobius3:
                self._widget.camera3_on.setStyleSheet("background-color: green")
                self._widget.camera3_off.setStyleSheet("background-color: grey")
            else:
                self._widget.camera3_off.setStyleSheet("background-color: green")
                self._widget.camera3_on.setStyleSheet("background-color: grey")

            if illumination:
                self._widget.ilumination_on.setStyleSheet("background-color: green")
                self._widget.ilumination_off.setStyleSheet("background-color: grey")
            else:
                self._widget.ilumination_off.setStyleSheet("background-color: green")
                self._widget.ilumination_on.setStyleSheet("background-color: grey")

            if tv_channel == 1:
                self._widget.tv1.setStyleSheet("background-color: green")
                self._widget.tv2.setStyleSheet("background-color: grey")
                self._widget.tv3.setStyleSheet("background-color: grey")
            elif tv_channel == 2:
                self._widget.tv1.setStyleSheet("background-color: grey")
                self._widget.tv2.setStyleSheet("background-color: green")
                self._widget.tv3.setStyleSheet("background-color: grey")
            elif tv_channel == 3:
                self._widget.tv1.setStyleSheet("background-color: grey")
                self._widget.tv2.setStyleSheet("background-color: grey")
                self._widget.tv3.setStyleSheet("background-color: green")
