#!/usr/bin/env python

import signal
import rospy
import sys

from core.MessageDispatcher import MessageDispatcher

def node():
    rospy.init_node('tracz_log_decoder')
    filename = rospy.get_param('~filename')

    message_dispacher = MessageDispatcher()
    message_dispacher.run_log_decoder(filename)

    rospy.spin()


if __name__ == '__main__':
    for sig in ('TERM', 'HUP', 'INT'):
        signal.signal(getattr(signal, 'SIG'+sig), quit)
    try:
        node()
    except rospy.ROSInterruptException:
        pass
