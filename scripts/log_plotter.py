#!/usr/bin/env python

import sys
from core.LogAnalyzer import plot_data_from_file

def node():
    if len(sys.argv)>1:
        file = sys.argv[1]
        yaxis = sys.argv[2]

        plot_data_from_file(file, yaxis)

    else:
        print("Not valid parameters")

if __name__ == '__main__':
    node()
