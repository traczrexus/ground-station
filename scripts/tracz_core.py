#!/usr/bin/env python

import signal
import threading
import time
import rospy

from core.MessageDispatcher import MessageDispatcher
from std_msgs.msg import Float64

def node():
    global message_dispacher #pylint: disable=global-variable-undefined,invalid-name
    rospy.init_node('tracz_core')
    port_name = rospy.get_param('~port', '/dev/tracz')

    timestamp_thread = threading.Thread(target=generate_timestamp)
    timestamp_thread.setDaemon(True)
    timestamp_thread.start()

    message_dispacher = MessageDispatcher(port_name)
    message_dispacher.run()

    rospy.spin()

def on_exit(sig, frame): #pylint: disable=unused-argument
    global message_dispacher #pylint: disable=global-variable-undefined,invalid-name,global-variable-not-assigned
    rospy.logwarn("Closing Ground Station")
    message_dispacher.close()

def generate_timestamp():
    timestamp_interval = 0.01 #seconds
    gs_time = 0
    starttime = time.time()
    timestamp_pub = rospy.Publisher('/ground_station/timestamp', Float64, queue_size=1)
    timestamp_msg = Float64()
    while True:
        timestamp_msg.data = gs_time
        timestamp_pub.publish(timestamp_msg)
        time.sleep(timestamp_interval
                   - ((time.time() - starttime) % timestamp_interval))
        gs_time += timestamp_interval

if __name__ == '__main__':
    signal.signal(signal.SIGINT, on_exit)

    try:
        node()
    except rospy.ROSInterruptException:
        pass
    except rospy.ROSException:
        pass
