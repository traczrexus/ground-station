#!/usr/bin/env bash

pylint --rcfile=git_scripts/pylint.rc scripts/core/*
pylint --rcfile=git_scripts/pylint.rc scripts/plugins/*
pylint --rcfile=git_scripts/pylint.rc scripts/tracz_core.py
