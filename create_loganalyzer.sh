#!/bin/bash

mkdir -p loganalyzer/ui
cp scripts/LogAnalyzerGUI.py loganalyzer/
cp scripts/tracz_types.py loganalyzer/
cp ui/log_analyzer.ui loganalyzer/ui
cp -r scripts/core/ loganalyzer/
cp requirements.txt loganalyzer/
cd loganalyzer
virtualenv env
source env/bin/activate
pip install -r requirements.txt
deactivate


